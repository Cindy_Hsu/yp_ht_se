.. _Ninja:

=====
Ninja
=====
:author: Furkan Ali Yurdakul
:date: 2 Dec 2020

.. image:: ninasphinx.png
   :width: 600
   :align: center

These team pages contain our findings during the project.

.. toctree::
   :glob:
   :maxdepth: 1

   */index
